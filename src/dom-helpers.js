export const createNode = (tagName, attributes = {}, children = []) => {
  const node = document.createElement(tagName)

  Object.keys(attributes).forEach(key => {
    node.setAttribute(key, attributes[key])
  })

  children.forEach(child => {
    node.appendChild(child)
  })

  return node
}

export const fragment = (children = []) => {
  const node = document.createDocumentFragment()

  children.forEach(child => {
    node.appendChild(child)
  })

  return node
}

export const text = text => document.createTextNode(text)
export const div = children => createNode('div', {}, children)
export const ul = children => createNode('ul', {}, children)
export const li = children => createNode('li', {}, children)
