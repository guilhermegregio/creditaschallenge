import uuid from 'uuid'

class Message {
  static fromString(message) {
    return new Message(message)
  }

  constructor(message) {
    this.id = uuid()
    this.message = message
  }

  getMessage() {
    return this.message
  }
}

export default Message
