import 'jest-dom/extend-expect'
import {getByText, getByTestId} from 'dom-testing-library'

import render from './helpers/render'
import {div, ul, li, text, fragment} from '../dom-helpers'

describe('DOM Helpers', () => {
  test('should create a div with text', () => {
    const container = render(div([text('test text')]))

    getByText(container, 'test text')
  })

  test('should create a with fragment', () => {
    const container = render(
      div([
        fragment([
          div([text('div1')]),
          div([text('div2')]),
          div([text('div3')]),
          div([text('div4')]),
        ]),
      ]),
    )

    getByText(container, 'div1')
    getByText(container, 'div2')
    getByText(container, 'div3')
    getByText(container, 'div4')

    expect(container.firstChild.childElementCount).toBe(4)
  })

  test('should create a ul with li and text', () => {
    const container = render(
      div([
        ul([
          li([text('Item 1')]),
          li([text('Item 2')]),
          li([text('Item 3')]),
          li([text('Item 4')]),
          li([text('Item 5')]),
        ]),
      ]),
    )

    getByText(container, 'Item 1')
    getByText(container, 'Item 2')
    getByText(container, 'Item 3')
    getByText(container, 'Item 4')
    getByText(container, 'Item 5')

    expect(container.querySelector('ul').childElementCount).toBe(5)
  })
})
