import chatService from '../chat-service'

describe('Chat Service', () => {
  let chatOne
  let chatTwo

  beforeEach(async () => {
    chatOne = await chatService.createChat('One')
    chatTwo = await chatService.createChat('Two')
    chatTwo.addMessage('message one')
    chatTwo.addMessage('message two')
  })

  test('should return all chats', async () => {
    const chats = await chatService.getAll()

    expect(chats.length).toBe(2)
  })

  test('should create a new new chat', async () => {
    const chat = await chatService.createChat('Test')

    expect(chat.id).toEqual(expect.any(String))
    expect(chat.name).toBe('Test')
  })

  test('should return a chat by id', async () => {
    const chat = await chatService.findChatBy(chatOne.id)

    expect(chat.name).toBe('One')
  })

  test('should return messages of chat by id', async () => {
    const messages = await chatService.getMessagesChatBy(chatTwo.id)

    expect(messages.length).toBe(2)
    expect(messages[0].getMessage()).toBe('message one')
    expect(messages[1].getMessage()).toBe('message two')
  })
})
