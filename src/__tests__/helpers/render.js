import {div} from '../../dom-helpers'

const render = element => {
  return div([element])
}

export default render
