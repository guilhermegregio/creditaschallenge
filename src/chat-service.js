import Chats from './chats-model'

const chats = new Chats()

class ChatService {
  getAll() {
    return new Promise(resolve => {
      resolve(chats.getChats())
    })
  }

  createChat(name) {
    return new Promise(resolve => {
      const chat = chats.addChat(name)

      resolve(chat)
    })
  }

  findChatBy(id) {
    return new Promise(resolve => {
      resolve(chats.findChat(id))
    })
  }

  getMessagesChatBy(id) {
    return new Promise(async resolve => {
      const chat = await this.findChatBy(id)

      resolve(chat.messages)
    })
  }
}

export default new ChatService()
