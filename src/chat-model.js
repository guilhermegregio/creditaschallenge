import uuid from 'uuid'
import Message from './message-model'

class Chat {
  static fromName(name) {
    return new Chat(name)
  }

  constructor(name, messages = []) {
    this.id = uuid()
    this.name = name || this.id
    this.messages = messages.map(message => Message.fromString(message))
  }

  getName() {
    return this.name
  }

  getMessages() {
    return this.messages
  }

  addMessage(message) {
    this.messages.push(Message.fromString(message))
  }
}

export default Chat
