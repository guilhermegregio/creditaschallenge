import {createNode, li, text, fragment} from './dom-helpers'
import Message from './message-model'
import chatService from './chat-service'

const template = createNode('template')

const liText = message => li([text(message)])

const styles = `
<style>
  .messages {
    width: 20em;
  }

  .message-list {
    min-height: 10em;
    background-color: #f3f3f3;
  }
</style>
`

template.innerHTML = `
  ${styles}
  <h1>Chat</h1>

  <div class="messages">
    <ul class="message-list"></ul>

    <form class="message-input">
      <input type="text" />
      <button type="submit">Enviar</button>
    </form>
  </div>
`

class ChatComponent extends HTMLElement {
  constructor() {
    super()

    this.sendMessage = this.sendMessage.bind(this)
    this.chatHandler = this.chatHandler.bind(this)

    this.attachShadow({mode: 'open'})
    this.shadowRoot.appendChild(template.content.cloneNode(true))

    this.chatTitle = this.shadowRoot.querySelector('h1')
    this.form = this.shadowRoot.querySelector('form')
    this.input = this.shadowRoot.querySelector('input')
    this.messageList = this.shadowRoot.querySelector('ul')
  }

  connectedCallback() {
    this.form.addEventListener('submit', this.sendMessage)

    chatService.findChatBy(this.chatId).then(this.chatHandler)
  }

  get chatId() {
    return this.getAttribute('chatId')
  }

  set chatId(newValue) {
    this.setAttribute('chatId', newValue)
  }

  chatHandler(chat) {
    const messages = chat.getMessages()

    this.chatTitle.innerText = chat.name
    const messagesHtml = messages.map(message => {
      return liText(message.getMessage())
    })

    this.messageList.appendChild(fragment(messagesHtml))
  }

  sendMessage(ev) {
    ev.preventDefault()
    const message = new Message(this.input.value)
    const messageHTML = liText(message.getMessage())

    this.messageList.appendChild(messageHTML)
    this.input.value = ''
    this.input.focus()
  }

  disconnectedCallback() {
    this.form.removeEventListener('submit', this.sendMessage)
  }
}

window.customElements.define('chat-component', ChatComponent)
