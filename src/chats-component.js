import {createNode} from './dom-helpers'
import chatService from './chat-service'

const template = createNode('template')

const styles = `
<style>
  button {
    padding: 10px;
  }
</style>
`

template.innerHTML = `
  ${styles}
  <button>Add chat</button>
  <div></div>
`

class ChatsComponent extends HTMLElement {
  constructor() {
    super()

    this.addChat = this.addChat.bind(this)
    this.addChatCallback = this.addChatCallback.bind(this)

    this.attachShadow({mode: 'open'})
    this.shadowRoot.appendChild(template.content.cloneNode(true))

    this.addChatButton = this.shadowRoot.querySelector('button')
    this.chatList = this.shadowRoot.querySelector('div')

    chatService.createChat('Default').then(chat => {
      chat.addMessage('Simulando mensagens antigas 1')
      chat.addMessage('Simulando mensagens antigas 2')

      this.addChatCallback(chat)
    })
  }

  connectedCallback() {
    this.addChatButton.addEventListener('click', this.addChat)
  }

  addChat(ev) {
    chatService.createChat().then(this.addChatCallback)
  }

  addChatCallback(chat) {
    this.chatList.appendChild(createNode('chat-component', {chatId: chat.id}))
  }

  disconnectedCallback() {
    this.addChatButton.removeEventListener('click', this.addChat)
  }
}

window.customElements.define('chats-component', ChatsComponent)
