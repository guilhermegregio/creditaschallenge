import Chat from './chat-model'

class Chats {
  constructor() {
    this.chats = []
  }

  addChat(name) {
    const chat = Chat.fromName(name)
    this.chats.push(chat)

    return chat
  }

  getChats() {
    return this.chats
  }

  findChat(id) {
    return this.chats.find(chat => id === chat.id)
  }
}

export default Chats
