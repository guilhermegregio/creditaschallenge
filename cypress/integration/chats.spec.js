describe('Chats', function() {
  context('Chats Page', () => {
    it('Visite page', () => {
      cy.visit('/')
    })

    it('Should add new chat', function() {
      cy.get('chats-component').then(shadowRoot => {
        Cypress.$(shadowRoot[0].shadowRoot)
          .find('button')
          .click()
      })

      cy.wait(100)

      cy.get('chats-component').then(shadowRoot => {
        const chatsContainer = Cypress.$(shadowRoot[0].shadowRoot).find(
          'div',
        )[0]

        expect(chatsContainer.childElementCount).to.equal(2)
      })
    })

    it('should add message', function() {
      cy.get('chats-component').then(shadowRoot => {
        const chat = Cypress.$(shadowRoot[0].shadowRoot).find(
          'chat-component',
        )[0].shadowRoot

        const inputMessage = Cypress.$(chat).find('input')[0]
        const sendButton = Cypress.$(chat).find('button')
        const listMessage = Cypress.$(chat).find('ul')[0]

        inputMessage.value = 'teste'

        sendButton.click()

        expect(listMessage.childElementCount).to.equal(3)
      })
    })
  })
})
